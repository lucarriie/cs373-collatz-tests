#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 2016
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.4/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve

# -----------
# TestCollatz
# -----------


class TestCollatz (TestCase):
    # ----
    # read
    # ----

    def test_read(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, 10)

    # testing for Value Error
    def test_read_2(self):
        s = "a b"
        with self.assertRaises(ValueError):
            collatz_read(s)

    # testing only first 3 inputs read
    def test_read_3(self):
        s = "1 10 3\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, 10)

    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = collatz_eval(1, 10)
        self.assertEqual(v, 20)

    def test_eval_2(self):
        v = collatz_eval(100, 200)
        self.assertEqual(v, 125)

    def test_eval_3(self):
        v = collatz_eval(201, 210)
        self.assertEqual(v, 89)

    def test_eval_4(self):
        v = collatz_eval(900, 1000)
        self.assertEqual(v, 174)

    # tesing i > j
    def test_eval_5(self):
        v = collatz_eval(10, 1)
        self.assertEqual(v, 20)

    # testing same number
    def test_eval_6(self):
        v = collatz_eval(10, 10)
        self.assertEqual(v, 7)

    # testing 1 as input
    def test_eval_7(self):
        v = collatz_eval(1, 1)
        self.assertEqual(v, 1)
    
    # testing big number
    def test_eval_8(self):
        v = collatz_eval(999999, 999999)
        self.assertEqual(v, 259)
    
    # test big range
    def test_eval_9(self):
        v = collatz_eval(1, 999999)
        self.assertEqual(v, 525)

    # -----
    # print
    # -----

    def test_print(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")

    def test_print_2(self):
        w = StringIO()
        collatz_print(w, 100, 200, 125)
        self.assertEqual(w.getvalue(), "100 200 125\n")

    def test_print_3(self):
        w = StringIO()
        collatz_print(w, 900, 1000, 174)
        self.assertEqual(w.getvalue(), "900 1000 174\n")

    # -----
    # solve
    # -----

    def test_solve(self):
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")

    def test_solve_2(self):
        r = StringIO("1 10 100 200 201 210 900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n")

    def test_solve_3(self):
        r = StringIO("209481 488897\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "209481 488897 449\n")
    
    # test corner case new line input
    def test_solve_4(self):
        r = StringIO("209481 488897\n\n1 10\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "209481 488897 449\n1 10 20\n")

# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
% coverage run --branch TestCollatz.py >  TestCollatz.out 2>&1
% cat TestCollatz.out
...................
----------------------------------------------------------------------
Ran 19 tests in 8.711s

OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          41      0     20      0   100%
TestCollatz.py      79      0      0      0   100%
------------------------------------------------------------
TOTAL              120      0     20      0   100%
"""